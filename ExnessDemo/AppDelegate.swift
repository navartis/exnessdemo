//
//  AppDelegate.swift
//  ExnessDemo
//
//  Created by DMITRY KULAKOV on 14/03/2017.
//  Copyright © 2017 Exness. All rights reserved.
//

import UIKit

//@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // Initialize DataModel if needed
        if DataModel.shared.allInstruments().count == 0 ||
            LaunchArguments.isForceInitializeDB() {
            let identifiers = Defaults.instruments
            DataModel.shared.initialize(identifiers: identifiers)
        }
        
        // Delete depricated data
        let curTimestamp = Date().timeIntervalSince1970
        let timestampForTickDeletting = curTimestamp - Defaults.timeToKeepTicks
        DataModel.shared.deleteTicks(olderThan: timestampForTickDeletting)
        
        // Tune up DataFetcher
        if LaunchArguments.isUseFakeDataFetcher() {
            SimpleFakeDataFetcher.shared.start()
        } else {
            DataFetcher.shared.start()
        }
        
        window?.tintColor = UIColor(colorLiteralRed: 0.996, green: 0.780, blue: 0.322, alpha: 1)
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        DataFetcher.shared.stop()
    }
}

