//
//  LaunchArguments.swift
//  ExnessDemo
//
//  Created by DMITRY KULAKOV on 16/03/2017.
//  Copyright © 2017 Exness. All rights reserved.
//

import Foundation

struct LaunchArgument{
    static let XTestDeactivateAppDelegate = "XTestDeactivateAppDelegate"
    static let XTestUseFakeDataFetcher = "XTestUseFakeDataFetcher"
    static let XTestForceInitializeDB = "XTestForceInitializeDB"
}

struct LaunchArguments {
    static func args() -> [String] {
        return ProcessInfo.processInfo.arguments
    }
    
    static func isArgExists(_ arg: String) -> Bool {
        return args().contains(arg)
    }

    static func isDeactivateAppDelegate() -> Bool {
        return isArgExists(LaunchArgument.XTestDeactivateAppDelegate)
    }
    
    static func isUseFakeDataFetcher() -> Bool {
        return isArgExists(LaunchArgument.XTestUseFakeDataFetcher)
    }
    
    static func isForceInitializeDB() -> Bool {
        return isArgExists(LaunchArgument.XTestForceInitializeDB)
    }
}
