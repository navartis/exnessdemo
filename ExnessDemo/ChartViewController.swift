//
//  ChartViewController.swift
//  ExnessDemo
//
//  Created by DMITRY KULAKOV on 17/03/2017.
//  Copyright © 2017 Exness. All rights reserved.
//

import UIKit
import Charts
import RealmSwift
import Realm

enum ChartDataSetIndex: Int {
    case bid = 0
    case ask
    
    static func count() -> Int {
        return ChartDataSetIndex.ask.int() + 1
    }
    
    func int() -> Int {
        return self.rawValue
    }
}

class ChartViewController: UIViewController {
    
    @IBOutlet weak var chartView: LineChartView!

    var productIdentifier: String!
    
    var ticks: Results<Tick>!
    var ticksToken: NotificationToken!
    
    var bidColor = NSUIColor.green
    var askColor = NSUIColor.red
    
    let initialDataSecondAgo = TimeInterval(60)
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configure data source
        ticks = DataModel.shared.ticks(identifier: self.productIdentifier,
                                       secondsAgo: initialDataSecondAgo)
        
        // Set scene title
        let title = InstrumentName.by(productIdentifier)
        setNavigationBarTitle(title: title)
        
        // Chart configuration
        configureChart()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        fullFillChartWithInitialData()
        
        startDataModelNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        stopDataModelNotifications()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - View UI staff
    
    func setNavigationBarTitle(title: String) {
        navigationItem.title = title
    }
    
    // MARK: - Chart management
    
    func configureChart() {
        configureChartAppearance()
        configureChartBehavior()
    }
    
    func configureChartAppearance() {
        chartView.chartDescription?.enabled = false
        chartView.drawGridBackgroundEnabled = true
        
        chartView.borderLineWidth = 1
        
        // xAxis
        chartView.xAxis.labelPosition = .bottom
        chartView.xAxis.valueFormatter = self
        
        // yAxis
        chartView.rightAxis.enabled = false
        chartView.leftAxis.valueFormatter = self
    }
    
    func configureChartBehavior() {
        chartView.dragEnabled = true
        chartView.scaleXEnabled = true
        chartView.scaleYEnabled = false
        chartView.pinchZoomEnabled = true
    }
    
    func fullFillChartWithInitialData() {
        
        var bidEntities: [ChartDataEntry] = []
        var askEntities: [ChartDataEntry] = []
        for tick in ticks {
            let tickTimestamp = tick.timestamp
            
            // BID
            guard let bid = tick.bid.value else {
                continue
            }
            let bidEntity = ChartDataEntry(x: tickTimestamp, y: bid)
            bidEntities.append(bidEntity)
            
            // ASK
            guard let ask = tick.ask.value else {
                continue
            }
            let askEntity = ChartDataEntry(x: tickTimestamp, y: ask)
            askEntities.append(askEntity)
        }
        
        var bidDataSet = LineChartDataSet(values: bidEntities, label: NSLocalizedString("BID", comment: ""))
        bidDataSet.setColor(bidColor, alpha: 1)
        bidDataSet.setCircleColor(bidColor)
        
        var askDataSet = LineChartDataSet(values: askEntities, label: NSLocalizedString("ASK", comment: ""))
        askDataSet.setColor(askColor, alpha: 1)
        askDataSet.setCircleColor(askColor)
        
        configureDataSetAppearance(dataSet: &bidDataSet)
        configureDataSetAppearance(dataSet: &askDataSet)
        
        var dataSets = Array(repeating: LineChartDataSet(),
                             count: ChartDataSetIndex.count())
        
        dataSets[ChartDataSetIndex.bid.int()] = bidDataSet
        dataSets[ChartDataSetIndex.ask.int()] = askDataSet
        
        chartView.data = LineChartData(dataSets: dataSets)
    }
    
    // MARK: - Chart Helpers
    
    func configureDataSetAppearance(dataSet: inout LineChartDataSet) {
        dataSet.lineWidth = 2
        dataSet.circleRadius = 3
        dataSet.drawCircleHoleEnabled = true
        dataSet.drawValuesEnabled = false
    }
    
    // MARK: - DataModel Notifications Management
    
    func startDataModelNotifications() {
        ticksToken = ticks.addNotificationBlock({
            [weak self] (changes) in
            
            switch changes {
            case .update(_, _, let insertions, _) :
                
                for tickIndex in insertions {
                    guard let tick = self?.ticks[tickIndex] else {
                        continue
                    }
                    
                    let tickTimestamp = tick.timestamp
                    
                    // BID
                    guard let bid = tick.bid.value else {
                        continue
                    }
                    let bidEntity = ChartDataEntry(x: tickTimestamp, y: bid)
                    self?.chartView.data?.addEntry(bidEntity, dataSetIndex: ChartDataSetIndex.bid.int())
                    
                    // ASK
                    guard let ask = tick.ask.value else {
                        continue
                    }
                    let askEntity = ChartDataEntry(x: tickTimestamp, y: ask)
                    self?.chartView.data?.addEntry(askEntity, dataSetIndex: ChartDataSetIndex.ask.int())
                    
                    self?.chartView.notifyDataSetChanged()
                }
            default:
                break
            }
        })
    }
    
    func stopDataModelNotifications() {
        ticksToken.stop()
    }
}

// MARK: - IAxisValueFormatter
extension ChartViewController: IAxisValueFormatter {
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        var axisValue = ""
        
        if axis is XAxis {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "mm:ss"
            axisValue = dateFormatter.string(from: Date(timeIntervalSince1970: value))
        } else if axis is YAxis {
            axisValue = String(format:"%.1f", value)
        }
        
        return axisValue
    }
}
