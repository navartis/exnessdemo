//
//  WSSubscribedResponse.swift
//  ExnessDemo
//
//  Created by DMITRY KULAKOV on 16/03/2017.
//  Copyright © 2017 Exness. All rights reserved.
//

import Foundation

public enum WSSubscribedResponseField: String, DictFieldEnum {
    case ticks = "ticks"
    case subscribedCount = "subscribed_count"
    case subscribedList = "subscribed_list"
    
    public func str() -> String {
        return self.rawValue
    }
}

struct WSSubscribedResponse {
    let ticks: [WSTick]
    
    init(dict: [String: Any]) throws {
        let subscribeCount = try dict.mandatoryValue(WSSubscribedResponseField.subscribedCount, Int.self)
        
        var ticksAr = [WSTick]()
        
        if subscribeCount > 0 {
            let rawSubscribedList = try dict.optionalValue(WSSubscribedResponseField.subscribedList)
            
            guard let subscribedList = rawSubscribedList as? [String: Any] else {
                throw DictObjectError.wrongFieldType(WSSubscribedResponseField.subscribedList,
                                                     rawSubscribedList)
            }
            
            guard let ticksDists = subscribedList["ticks"] as? [[String: Any]] else {
                throw DictObjectError.wrongFieldType(WSSubscribedResponseField.subscribedList,
                                                     rawSubscribedList)
            }
            
            for dict in ticksDists {
                let tick = try WSTick(dict: dict)
                ticksAr.append(tick)
            }
        }
        
        self.ticks = ticksAr
    }
    
    init(json: String) throws {
        let dict = try json.dict()
        try self.init(dict: dict)
    }
}
