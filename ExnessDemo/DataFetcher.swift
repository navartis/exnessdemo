//
//  DataFetcher.swift
//  ExnessDemo
//
//  Created by DMITRY KULAKOV on 16/03/2017.
//  Copyright © 2017 Exness. All rights reserved.
//

import Foundation
import SwiftWebSocket
import RealmSwift

/**
 DataFetcher получает данные из Web Socket и добавляет их в DataModel
 */
class DataFetcher {
    let stopCode = 7777
    
    let dataModel: DataModel
    let wsURL: String
    let ws: WebSocket
    
    var instruments: Results<Instrument>!
    var notificationToken: NotificationToken!
    
    static let shared: DataFetcher = {
        let dataModel = DataModel.shared
        return DataFetcher(dataModel: dataModel, wsURL: Defaults.wsURL)
    }()
    
    
    /// Инициализирует DataFetcher.
    ///
    /// - Parameters:
    ///     - dataModel: Модель данных.
    ///     - wsURL: Адрес веб сокета.
    init(dataModel: DataModel, wsURL: String) {
        self.dataModel = dataModel
        self.instruments = dataModel.allInstruments()
        
        self.wsURL = wsURL
        
        self.ws = WebSocket()
        self.configureWS()
    }
    
    /// Конфигурирует сокет
    func configureWS() {
        weak var weakSelf = self
        
        ws.event.open = {
            // print("ws opened")
            weakSelf?.updateSubscriptions()
        }
        
        ws.event.close = { code, reason, clean in
            // print("ws closed: \(code) \(reason) \(clean)")
            
            if code == weakSelf?.stopCode {
                return
            }
            
            weakSelf?.ws.open() // reopen
        }
        
        ws.event.error = { error in
            // print("ws error: \(error)")
            weakSelf?.ws.open() // reopen
        }
        
        ws.event.message = { message in
            // print(messages)
            weakSelf?.processReceivedMessage(message)
        }
    }
    
    /// Инициирует открытие сокета и заргрузку данных.
    func start() {
        ws.open(wsURL)
        
        startDataModelNotifications()
    }
    
    /// Инициирует закрытие сокета и остановку заргрузки данных.
    func stop() {
        ws.close(stopCode)
        
        stopDataModelNotifications()
    }
    
    /// Добавляет котировки в подписку на получение данных.
    ///
    /// - Parameters:
    ///     - identifiers: Идентификаторы котировок.
    func subscribe(identifiers: [String]) {
        if identifiers.count == 0 {
            return
        }
        
        let msg = WebSocketMsgBuilder.subscribe(identifiers: identifiers)
        // print(msg)
        ws.send(msg)
    }
    
    /// Удаляет котировки из подписку на получение данных.
    ///
    /// - Parameters:
    ///     - identifiers: Идентификаторы котировок.
    func unsubscribe(identifiers: [String]) {
        if identifiers.count == 0 {
            return
        }
        
        let msg = WebSocketMsgBuilder.unsubscribe(identifiers: identifiers)
//        print(msg)
        ws.send(msg)
    }
    
    /// Обновляет состояние подписки согласно состоянию DataModel
    func updateSubscriptions() {
        var enabledIdentifiers = [String]()
        var disabledIdentifiers = [String]()
        for instrument in instruments {
            let identifier = instrument.identifier
            if instrument.enabled {
                enabledIdentifiers.append(identifier)
            } else {
                disabledIdentifiers.append(identifier)
            }
        }
        
        self.unsubscribe(identifiers: disabledIdentifiers)
        self.subscribe(identifiers: enabledIdentifiers)
    }
    
    /// Обрабатывает сообщения, полученные из сокета.
    func processReceivedMessage(_ message: Any) {
        guard let json = message as? String else {
            return
        }
        
        var ticks = [WSTick]()
        if let response = try? WSSubscribedResponse(json: json) {
            ticks = response.ticks
        } else if let response = try? WSTiksResponse(json: json) {
            ticks = response.ticks
        }
        
        dataModel.addTicks(ticks)
    }
    
    /// Подписывает на получение уведомлений об изменении данных в DataModel
    /// для изменения состояния подписки.
    func startDataModelNotifications() {
        notificationToken = instruments.addNotificationBlock({
            [weak self] (changes) in

            switch changes {
            case .initial:
                break
            case .update(_, _, _, let modifications):
                
                var enabledIdentifiers = [String]()
                var disabledIdentifiers = [String]()
                
                for index in modifications {
                    if let instrument = self?.instruments[index] {
                        let instIdentifier = instrument.identifier
                        if instrument.enabled {
                            enabledIdentifiers.append(instIdentifier)
                        } else {
                            disabledIdentifiers.append(instIdentifier)
                        }
                    }
                }
                
                self?.unsubscribe(identifiers: disabledIdentifiers)
                self?.subscribe(identifiers: enabledIdentifiers)

                break
            case .error(let error):
                fatalError("\(error)")
                break
            }
        })
    }
    
    /// Отписывает от получения уведомлений об изменении данных в DataModel.
    func stopDataModelNotifications() {
        notificationToken.stop()
    }
    
}
