//
//  InstrumentName.swift
//  ExnessDemo
//
//  Created by DMITRY KULAKOV on 16/03/2017.
//  Copyright © 2017 Exness. All rights reserved.
//

import Foundation


struct InstrumentName {
    
    /// Длинна валидного идентификатора инструмента
    static let validLength = 6
    
    /// Формирует название инструмента
    ///
    /// - Parameters:
    ///     - identifier: Идентификатор инструмента
    /// - Returns: Наименование инструмента
    static func by(_ identifier: String) -> String {
        if identifier.characters.count != validLength {
            return identifier
        }
        
        var name = identifier
        let insertionIndex = identifier.index(identifier.startIndex,
                                              offsetBy: 3)
        name.insert("/", at: insertionIndex)
        
        return name
    }
}
