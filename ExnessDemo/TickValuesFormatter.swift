//
//  TickValuesFormatter.swift
//  ExnessDemo
//
//  Created by DMITRY KULAKOV on 18/03/2017.
//  Copyright © 2017 Exness. All rights reserved.
//

import Foundation

/// Преобразователь основных показателей тика в строки
struct TickValuesFormatter {
    
    /// Преобразует значение Double в строку фиксированной длинны
    ///
    /// - Parameters:
    ///     - value: Значение Double
    ///     - length: Длинна строки результата
    /// - Returns: Строку фиксированной длинны
    static func string(_ value: Double, length: UInt) -> String {
        let rawString = String(format:"%.\(length)f", value)
        
        let clearStr = rawString.padding(toLength: Int(length),
                                         withPad: "",
                                         startingAt: 0)
        
        return clearStr
    }
    
    /// Формирует строку значения BID тика
    ///
    /// - Parameters:
    ///     - value:  Значение параметра
    ///     - length: Длинна строки, которую надо вернуть
    /// - Returns: Строку значения BID
    static func bid(_ value: Double, length: UInt = 7) -> String {
        return string(value, length: length)
    }
    
    /// Формирует строку значения ASK тика
    ///
    /// - Parameters:
    ///     - value:  Значение параметра
    ///     - length: Длинна строки, которую надо вернуть
    /// - Returns: Строку значения ASK
    static func ask(_ value: Double, length: UInt = 7) -> String {
        return string(value, length: length)
    }
    
    /// Формирует строку значения Spread тика
    ///
    /// - Parameters:
    ///     - value:  Значение параметра
    ///     - length: Длинна строки, которую надо вернуть
    /// - Returns: Строку значения Spread
    static func spread(_ value: Double, length: UInt = 3) -> String {
        return string(value, length: length)
    }
}
