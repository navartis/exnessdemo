//
//  WSTiksResponse.swift
//  ExnessDemo
//
//  Created by DMITRY KULAKOV on 16/03/2017.
//  Copyright © 2017 Exness. All rights reserved.
//

import Foundation

public enum WSTicksField: String, DictFieldEnum {
    case ticks = "ticks"
    
    public func str() -> String {
        return self.rawValue
    }
}

struct WSTiksResponse {
    let ticks: [WSTick]

    init(dict: [String: Any]) throws {
        
        let rawTiks = try dict.optionalValue(WSTicksField.ticks)
        
        guard let tiksDicts = rawTiks as? [[String: Any]] else {
            throw DictObjectError.wrongFieldType(WSTicksField.ticks, rawTiks)
        }
        
        var ticksAr = [WSTick]()
        
        for dict in tiksDicts {
            let tick = try WSTick(dict: dict)
            ticksAr.append(tick)
        }
        
        self.ticks = ticksAr
    }
    
    init(json: String) throws {
        let dict = try json.dict()
        try self.init(dict: dict)
    }
}
