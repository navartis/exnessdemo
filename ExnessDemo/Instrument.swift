//
//  Instrument.swift
//  ExnessDemo
//
//  Created by DMITRY KULAKOV on 14/03/2017.
//  Copyright © 2017 Exness. All rights reserved.
//

import Foundation
import RealmSwift

enum InstrumentProp: String {
    case identifier = "identifier"
    case enabled = "enabled"
    case position = "position"
}

class Instrument: Object {
    dynamic var identifier = ""
    dynamic var enabled = false
    dynamic var position: Int = -1
    dynamic var lastTick: Tick?
    let ticks = List<Tick>()
    
    override static func primaryKey() -> String? {
        return InstrumentProp.identifier.rawValue
    }
    
    convenience init(identifier: String, enabled: Bool, position: Int) {
        self.init()
        
        self.identifier = identifier
        self.enabled = enabled
        self.position = position
    }
}
