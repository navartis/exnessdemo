//
//  UIStoryboard+main.swift
//  ExnessDemo
//
//  Created by DMITRY KULAKOV on 15/03/2017.
//  Copyright © 2017 Exness. All rights reserved.
//

import Foundation
import UIKit

extension UIStoryboard {
    class func main() -> UIStoryboard {
        return UIStoryboard(name: "Main", bundle: nil)
    }
}
