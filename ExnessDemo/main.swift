//
//  main.swift
//  ExnessDemo
//
//  Created by DMITRY KULAKOV on 16/03/2017.
//  Copyright © 2017 Exness. All rights reserved.
//

import UIKit

private func delegateClassName() -> String? {
    return LaunchArguments.isDeactivateAppDelegate() ? nil: NSStringFromClass(AppDelegate.self)
}

let unsafeArgv = UnsafeMutableRawPointer(CommandLine.unsafeArgv).bindMemory(
    to: UnsafeMutablePointer<Int8>.self,
    capacity: Int(CommandLine.argc))

UIApplicationMain(CommandLine.argc, unsafeArgv, nil, delegateClassName())
