//
//  SettingsViewController.swift
//  ExnessDemo
//
//  Created by DMITRY KULAKOV on 15/03/2017.
//  Copyright © 2017 Exness. All rights reserved.
//

import UIKit
import RealmSwift

class SettingsViewController: UITableViewController {

    var instruments: Results<Instrument>!
    var notificationToken: NotificationToken!
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configure data source
        instruments = DataModel.shared.allInstruments()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        notificationToken = instruments.addNotificationBlock({
            [weak self] (changes) in
            
            guard let tableView = self?.tableView else { return }
            switch changes {
            case .initial:
                tableView.reloadData()
                break
            case .update(_, let deletions, let insertions, let modifications):
                tableView.beginUpdates()
                tableView.insertRows(at: insertions.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.deleteRows(at: deletions.map({ IndexPath(row: $0, section: 0)}),
                                     with: .automatic)
                tableView.reloadRows(at: modifications.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.endUpdates()
                break
            case .error(let error):
                fatalError("\(error)")
                break
            }
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        notificationToken.stop()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

// MARK: - UITableViewDataSource 

extension SettingsViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return instruments.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        let instrument = instruments[indexPath.row]
        let instrumentName = InstrumentName.by(instrument.identifier)
        
        cell.textLabel?.text = instrumentName
        var accessoryType = UITableViewCellAccessoryType.none
        if instrument.enabled {
            accessoryType = .checkmark
        }
        cell.accessoryType = accessoryType
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let instrument = instruments[indexPath.row]
        DataModel.shared.toggleInstrumentEnabled(identifier: instrument.identifier)
    }
}
