//
//  Ticks.swift
//  ExnessDemo
//
//  Created by DMITRY KULAKOV on 16/03/2017.
//  Copyright © 2017 Exness. All rights reserved.
//

import Foundation
import RealmSwift

enum TickProp: String {
    case identifier = "identifier"
    case timestamp = "timestamp"
    case bid = "bid"
    case ask = "ask"
    case spread = "spread"
}

class Tick: Object {
    dynamic var identifier = ""
    dynamic var timestamp = Double(0)
    let bid = RealmOptional<Double>()
    let ask = RealmOptional<Double>()
    let spread = RealmOptional<Double>()
    
    override static func indexedProperties() -> [String] {
        return [TickProp.identifier.rawValue]
    }
    
    convenience init(identifier: String, timestamp: TimeInterval, bid: Double, ask: Double, spread: Double) {
        self.init()
        
        self.identifier = identifier
        self.timestamp = timestamp
        self.bid.value = bid
        self.ask.value = ask
        self.spread.value = spread
    }
}

enum LastTickProp: String {
    case enabled = "enabled"
    case position = "position"
}

class LastTick: Tick {
    dynamic var enabled = false
    dynamic var position: Int = -1
    
    convenience init(identifier: String, enabled: Bool, position: Int) {
        self.init()
        
        self.identifier = identifier
        self.enabled = enabled
        self.position = position
    }
    
    override static func primaryKey() -> String? {
        return TickProp.identifier.rawValue
    }
}
