//
//  VCBuilder.swift
//  ExnessDemo
//
//  Created by DMITRY KULAKOV on 15/03/2017.
//  Copyright © 2017 Exness. All rights reserved.
//

import Foundation
import UIKit

/// Билдер вью контроллеров
struct VCBuilder {
    
    /// Возвращает вью контроллер сцены настроек
    ///
    /// - Returns: Вью контроллер сцены настроек
    static func settings() -> SettingsViewController? {
        let identifier = String(describing: SettingsViewController.self)
        let rawVC = mainStoryboardVC(identifier: identifier)
        
        guard let vc = rawVC as? SettingsViewController else {
            return nil
        }
        
        return vc
    }
    
    /// Возвращает вью контроллер сцены графика
    ///
    /// - Returns: Вью контроллер сцены графика
    static func chart(_ productIdentifier: String) -> ChartViewController? {
        let identifier = String(describing: ChartViewController.self)
        let rawVC = mainStoryboardVC(identifier: identifier)
        
        guard let vc = rawVC as? ChartViewController else {
            return nil
        }
        
        vc.productIdentifier = productIdentifier
        
        return vc
    }
    
    /// Возвращает вью контроллер главной сцены
    ///
    /// - Returns: Вью контроллер главной сцены
    static func mainStoryboardVC(identifier: String) -> UIViewController {
        let storyboard = UIStoryboard.main()
        let rawVC = storyboard.instantiateViewController(withIdentifier: identifier)
        
        return rawVC
    }
}
