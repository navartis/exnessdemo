//
//  Defaults.swift
//  ExnessDemo
//
//  Created by DMITRY KULAKOV on 14/03/2017.
//  Copyright © 2017 Exness. All rights reserved.
//

import Foundation

/// Значения по умолчанию для основных конфигурационных параметров приложения
struct Defaults {
    /// Адрес веб сокета
    static let wsURL = "wss://quotes.exness.com:18400/"
    
    /// Идентификаторы инструментов
    static let instruments = ["EURUSD", "EURGBP", "USDJPY", "GBPUSD", "USDCHF", "USDCAD", "AUDUSD", "EURJPY", "EURCHF"]
    
    static let timeToKeepTicks = TimeInterval(60*60*24*3) // 3 days
}
