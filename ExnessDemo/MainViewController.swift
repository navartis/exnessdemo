//
//  MainViewController.swift
//  ExnessDemo
//
//  Created by DMITRY KULAKOV on 15/03/2017.
//  Copyright © 2017 Exness. All rights reserved.
//

import UIKit
import RealmSwift

class MainViewController: UITableViewController {
    
    var ticks: Results<LastTick>!
    var notificationToken: NotificationToken!
    
    var editModeBarButton: UIBarButtonItem!
    
    var tickCellReuseIdentifier: String!
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configure data source
        ticks = DataModel.shared.enabledLastTicks()
        
        // Register cells
        let tickCellNibName = String(describing: LastTickCell.self)
        tickCellReuseIdentifier = tickCellNibName
        
        let tickCellNib = UINib(nibName: tickCellNibName, bundle: nil)
        tableView.register(tickCellNib, forCellReuseIdentifier: tickCellReuseIdentifier)
        
        // Configure UI
        addBarButtons()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        startDataModelNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        stopDataModelNotifications()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        setEditing(editing: false, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - View UI staff
    
    func addBarButtons() {
        let settingsBarButton = UIBarButtonItem(title: NSLocalizedString("Settings", comment: ""),
                                                style: .plain,
                                                target: self,
                                                action: #selector(settingsBarButtonTapped(_:)))
        navigationItem.leftBarButtonItem = settingsBarButton
        
        editModeBarButton = UIBarButtonItem(title: NSLocalizedString("Edit", comment: ""),
                                            style: .plain,
                                            target: self,
                                            action: #selector(editModeBarButtonTapped(_:)))
        
        navigationItem.rightBarButtonItem = editModeBarButton
    }
    
    func setEditing(editing: Bool, animated: Bool) {
        tableView.setEditing(editing, animated: animated)
        
        let buttonTitle = editing ? NSLocalizedString("Done", comment: "") : NSLocalizedString("Edit", comment: "")
        editModeBarButton.title = buttonTitle
    }
    
    //MARK: - UI Actions
    
    func settingsBarButtonTapped(_ sender: UIBarButtonItem) {
        let settingsVC = VCBuilder.settings()!
        self.navigationController!.pushViewController(settingsVC, animated: true)
    }
    
    func editModeBarButtonTapped(_ sender: UIBarButtonItem) {
        let isEditing = tableView.isEditing
        let newEditingValue = !isEditing
        
        if newEditingValue == true {
            stopDataModelNotifications()
        } else {
            startDataModelNotifications()
        }
        
        setEditing(editing: newEditingValue, animated: true)
    }

    // MARK: - DataModel Notifications Management
    
    func startDataModelNotifications() {
        notificationToken = ticks.addNotificationBlock({
            [weak self] (changes) in
            
            guard let tableView = self?.tableView else { return }
            switch changes {
            case .initial:
                tableView.reloadData()
                break
            case .update(_, let deletions, let insertions, let modifications):
                tableView.beginUpdates()
                tableView.insertRows(at: insertions.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.deleteRows(at: deletions.map({ IndexPath(row: $0, section: 0)}),
                                     with: .automatic)
                tableView.reloadRows(at: modifications.map({ IndexPath(row: $0, section: 0) }),
                                     with: .none)
                tableView.endUpdates()
                break
            case .error(let error):
                fatalError("\(error)")
                break
            }
        })
    }
    
    func stopDataModelNotifications() {
        self.notificationToken.stop()
    }
}

// MARK: - UITableViewDataSource

extension MainViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ticks.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tickCellReuseIdentifier,
                                                 for: indexPath)
        
        let tick = ticks[indexPath.row]
        let instrumentName = InstrumentName.by(tick.identifier)
        
        var bid: String?
        if let bidDouble = tick.bid.value {
            bid = TickValuesFormatter.bid(bidDouble)
        }
        
        var ask: String?
        if let askDouble = tick.ask.value {
            ask = TickValuesFormatter.ask(askDouble)
        }
        
        var spread: String?
        if let spreadDouble = tick.spread.value {
            spread = TickValuesFormatter.spread(spreadDouble)
        }
        
        let tickCell = cell as! LastTickCell
        
        let n_a = NSLocalizedString("n/a", comment: "")
        
        tickCell.titleLabel.text = instrumentName
        tickCell.bidLabel.text = bid ?? n_a
        tickCell.askLabel.text = ask ?? n_a
        tickCell.spreadLabel.text = spread ?? n_a
        
        return tickCell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tick = ticks[indexPath.row]
        
        let productIdentifier = tick.identifier
        
        if let chartVC = VCBuilder.chart(productIdentifier) {
            navigationController?.pushViewController(chartVC, animated: true)
        }
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            let tick = ticks[indexPath.row]
            DataModel.shared.setInstrument(identifier: tick.identifier, enabled: false)
            
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        DataModel.shared.changeLastTickPosition(from: fromIndexPath.row,
                                                to: to.row)
    }
    
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}
