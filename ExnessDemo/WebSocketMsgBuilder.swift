//
//  WebSocketMsgBuilder.swift
//  ExnessDemo
//
//  Created by DMITRY KULAKOV on 16/03/2017.
//  Copyright © 2017 Exness. All rights reserved.
//

import Foundation

enum WebSocketCommang: String {
    case subscribe = "SUBSCRIBE"
    case unsubscribe = "UNSUBSCRIBE"
    
    func str() -> String {
        return self.rawValue
    }
}

struct WebSocketMsgBuilder {
    static let identifiersSeparator = ","
    
    static func subscribe(identifiers: [String]) -> String {
        return msg(command: .subscribe, identifiers: identifiers)
    }
    
    static func unsubscribe(identifiers: [String]) -> String {
        return msg(command: .unsubscribe, identifiers: identifiers)
    }
    
    static func msg(command: WebSocketCommang, identifiers: [String]) -> String {
        let identifiersList = identifiers.joined(separator: identifiersSeparator)
        
        let msg = "\(command.str()): \(identifiersList)"
        return msg
    }
}
