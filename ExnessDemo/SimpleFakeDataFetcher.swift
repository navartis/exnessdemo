//
//  SimpleFakeDataFetcher.swift
//  ExnessDemo
//
//  Created by DMITRY KULAKOV on 18/03/2017.
//  Copyright © 2017 Exness. All rights reserved.
//

import Foundation
import RandomKit

/// Фейковый загрузчик данных для работы над приложением в дни когда торговля не ведется.
class SimpleFakeDataFetcher {
    
    let dataModel: DataModel
    let productIdentifiers: [String]
    
    var timer: Timer?
    let timerTimeInterval = TimeInterval(0.3)
    
    static let shared: SimpleFakeDataFetcher = {
        return SimpleFakeDataFetcher(dataModel: DataModel.shared,
                                     productIdentifiers: Defaults.instruments)
    }()
    
    init(dataModel: DataModel, productIdentifiers: [String]) {
        self.dataModel = dataModel
        self.productIdentifiers = productIdentifiers
    }
    
    func start() {
        stop()
        self.timer = Timer.scheduledTimer(withTimeInterval: timerTimeInterval, repeats: true, block: { [weak self] (_) in
            self?.generateFakeTicks()
        })
    }
    
    func stop() {
        timer?.invalidate()
    }
    
    func generateFakeTicks() {
        var selectedIdentifiers = [String]()
        
        for identifier in self.productIdentifiers {
            if Bool.random(using: &Xoroshiro.default) {
                selectedIdentifiers.append(identifier)
            }
        }
        
        var ticks = [WSTick]()
        for identifier in selectedIdentifiers {
            let baseValue = productIdentifiers.index(of: identifier)!
            
            let tick = WSTick(identifier: identifier,
                              bid: randomBid(base: baseValue),
                              ask: randomAsk(base: baseValue),
                              spread: randomSpread())
            
            ticks.append(tick)
        }
        
        dataModel.addTicks(ticks)
    }
    
    func randomBid(base: Int) -> Double {
        let random = Double.random(within: 0...1, using: &Xoroshiro.default)
        let value = Double(base) + random
        
        return value
    }
    
    func randomAsk(base: Int) -> Double {
        let random = Double.random(within: -1...0, using: &Xoroshiro.default)
        let value = Double(base) + random
        
        return value
    }
    
    func randomSpread() -> Double {
        return Double.random(within: 0...3, using: &Xoroshiro.default)
    }
}
