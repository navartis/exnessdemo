//
//  DataModel.swift
//  ExnessDemo
//
//  Created by DMITRY KULAKOV on 14/03/2017.
//  Copyright © 2017 Exness. All rights reserved.
//

import Foundation
import RealmSwift

/// Модель данных приложения.
class DataModel {
    let realm: Realm
    
    static let shared: DataModel = {
        let defaultRealm = try! Realm()
        return DataModel(realm: defaultRealm)
    }()
    
    /// Инстанциирует модель.
    ///
    /// - Parameters:
    ///     - realm: Сконфигурированный и готовый к использованию Realm.
    init (realm: Realm) {
        self.realm = realm
    }
    
    // MARK: - Staff
    
    /// Инициализирует данные в модели.
    /// Удаляет все и добавляет исходные.
    ///
    /// - Parameters:
    ///     - identifiers: Идентификаторы инструментов
    func initialize(identifiers: [String]) {
        try! realm.write {
            realm.deleteAll()
            
            for (position, identifier) in identifiers.enumerated() {
                let instrument = Instrument(identifier: identifier,
                                            enabled: true,
                                            position: position)
                
                let lastTick = LastTick(identifier: identifier,
                                        enabled: true,
                                        position: position)
                
                realm.add(instrument)
                realm.add(lastTick)
            }
        }
    }
    
    // MARK: - Updates
    
    /// Изменяет состояние вкл/выкл для инструмента
    ///
    /// - Parameters:
    ///     - identifier: Идентификатор инструмента
    ///     - enabled: вкл или выкл
    func setInstrument(identifier: String, enabled: Bool) {
        guard let instrument = instrument(identifier: identifier) else {
            return
        }
        
        try! realm.write {
            instrument.enabled = enabled
            
            if let lastTick = self.lastTick(identifier: identifier) {
                lastTick.enabled = enabled
            }
            
            if !enabled {
                realm.delete(unorderedTicks(identifier: identifier))
            }
        }
    }
    
    /// Изменяет текущее состояние вкл/выкл на противоположное
    ///
    /// - Parameters:
    ///     - identifier: Идентификатор инструмента
    func toggleInstrumentEnabled(identifier: String) {
        guard let instrument = instrument(identifier: identifier) else {
            return
        }
        
        try! realm.write {
            instrument.enabled = !instrument.enabled
            
            if let lastTick = self.lastTick(identifier: identifier) {
                lastTick.enabled = instrument.enabled
            }
            
            if !instrument.enabled {
                realm.delete(unorderedTicks(identifier: identifier))
            }
        }
    }
    
    /// Изменяет значение `position` `LastTick`
    ///
    /// - Parameters:
    ///     - from: Исходное значение position
    ///     - to:   Новое значение position
    func changeLastTickPosition(from: Int, to: Int ) {
        if from == to {
            return;
        }
        
        let isDown = (to > from)
        let betweensPositionСhange = isDown ? -1 : 1
        let betweensSelectHeaderCondition = isDown ? ">" : "=>"
        let betweensSelectFooterCondition = isDown ? "<=" : "<"
        
        try! realm.write {
            guard let fromTick = self.lastTick(position: from) else {
                return
            }
            
            guard let maxPosition = [from, to].max(),
                let minPosition = [from, to].min() else {
                    return
            }
            
            let predicateFormat = "\(LastTickProp.position.rawValue) \(betweensSelectHeaderCondition) \(minPosition) AND \(LastTickProp.position.rawValue) \(betweensSelectFooterCondition) \(maxPosition)"
            
            let predicate = NSPredicate(format: predicateFormat)
            let filtered = realm.objects(LastTick.self).filter(predicate)
            
            for tick in filtered {
                tick.position = tick.position + betweensPositionСhange
            }
            
            fromTick.position = to
        }
    }

    // MARK: - Inserts
    
    /// Добавляет тики
    ///
    /// - Parameters:
    ///     - ticks: Тики
    func addTicks(_ ticks: [WSTick]) {
        if ticks.count == 0 { return }
        
        try! realm.write {
            for wsTick in ticks {
                if let lastTick = self.lastTick(identifier: wsTick.identifier) {
                    lastTick.bid.value = wsTick.bid
                    lastTick.ask.value = wsTick.ask
                    lastTick.spread.value = wsTick.spread
                }
                
                let tick = Tick(identifier: wsTick.identifier,
                                timestamp: Date().timeIntervalSince1970,
                                bid: wsTick.bid,
                                ask: wsTick.ask,
                                spread: wsTick.spread)
                
                realm.add(tick)
            }
        }
    }
    
    // MARK: - Queries
    
    /// Возвращает все инструменты, отсортированные по `position`
    ///
    /// - Returns: Выборку, содержащую отсортированные элементы
    func allInstruments() -> Results<Instrument> {
        let instruments = realm.objects(Instrument.self)
        let sorted = instruments.sorted(byKeyPath: InstrumentProp.position.rawValue, ascending: true)
        return sorted
    }

    /// Возвращает инструменты, помеченные как 'вкл' отсортированные по `position`
    ///
    /// - Returns: Выборку, содержащую отсортированные элементы
    func enabledInstruments() -> Results<Instrument> {
        let predicateFormat = "\(InstrumentProp.enabled.rawValue) == %@"
        let predicate = NSPredicate(format: predicateFormat,
                                    NSNumber(booleanLiteral: true))
        
        let all = realm.objects(Instrument.self)
        let filtered = all.filter(predicate)
        let sorted = filtered.sorted(byKeyPath: InstrumentProp.position.rawValue, ascending: true)
        
        return sorted
    }
    
    /// Возвращает инструмент
    ///
    /// - Parameters:
    ///     - identifier: Идентификатор инструмента
    func instrument(identifier: String) -> Instrument? {
        let predicateFormat = "\(InstrumentProp.identifier.rawValue) == %@"
        
        let predicate = NSPredicate(format: predicateFormat, identifier)
        
        let filtered = realm.objects(Instrument.self).filter(predicate)
        return filtered.first
    }
    
    /// Возвращает все последние тики, отсортированные по `position`
    ///
    /// - Returns: Выборку, содержащую отсортированные элементы
    func allLastTicks() -> Results<LastTick> {
        let ticks = realm.objects(LastTick.self)
        let sorted = ticks.sorted(byKeyPath: LastTickProp.position.rawValue, ascending: true)
        return sorted
    }
    
    /// Возвращает все последние тики, помеченные как 'вкл' отсортированные по `position`
    ///
    /// - Returns: Выборку, содержащую отсортированные элементы
    func enabledLastTicks() -> Results<LastTick> {
        let predicateFormat = "\(LastTickProp.enabled.rawValue) == %@"
        let predicate = NSPredicate(format: predicateFormat,
                                    NSNumber(booleanLiteral: true))
        
        let all = realm.objects(LastTick.self)
        let filtered = all.filter(predicate)
        let sorted = filtered.sorted(byKeyPath: LastTickProp.position.rawValue, ascending: true)
        
        return sorted
    }
    
    /// Возвращает выборку последнего тика
    ///
    /// - Parameters:
    ///     - identifier: Идентификатор продукта
    /// - Returns: Выборку, содержащую последний тик
    func lastTickResult(identifier: String) -> Results<LastTick> {
        let predicateFormat = "\(TickProp.identifier.rawValue) == %@"
        
        let predicate = NSPredicate(format: predicateFormat, identifier)
        
        let filtered = realm.objects(LastTick.self).filter(predicate)
        return filtered
    }
    
    /// Возвращает последний тик
    ///
    /// - Parameters:
    ///     - identifier: Идентификатор продукта
    /// - Returns: Последний тик
    func lastTick(identifier: String) -> LastTick? {
        let filtered = lastTickResult(identifier: identifier)
        return filtered.first
    }

    /// Возвращает последний тик
    ///
    /// - Parameters:
    ///     - position: Позиция
    /// - Returns: Последний тик
    func lastTick(position: Int) -> LastTick? {
        let predicateFormat = "\(LastTickProp.position.rawValue) == \(position)"
        
        let predicate = NSPredicate(format: predicateFormat)
        
        let filtered = realm.objects(LastTick.self).filter(predicate)
        return filtered.first
    }
    
    /// Возвращает отсортированные тики для конкретного продукта,
    /// с возвожностью указанния какие значения из прошлого нужны
    ///
    /// - Parameters:
    ///     - identifier: Идентификатор продукта
    ///     - secondsAgo: Количество секунд для захвата значения из прошлого
    /// - Returns: Выборку тиков
    func ticks(identifier: String, secondsAgo: TimeInterval) -> Results<Tick> {
        let startTimestamp = Date().timeIntervalSince1970 - secondsAgo
        let predicateFormat = "\(TickProp.identifier.rawValue) == %@ AND \(TickProp.timestamp.rawValue) > %@"
        let predicate = NSPredicate(format: predicateFormat,
                                    identifier, NSNumber(floatLiteral: startTimestamp))
        
        let all = realm.objects(Tick.self)
        let filtered = all.filter(predicate)
        let sorted = filtered.sorted(byKeyPath: TickProp.timestamp.rawValue, ascending: true)
        
        return sorted
    }
    
    /// Возвращает НЕ отсортированные тики для конкретного продукта
    ///
    /// - Parameters:
    ///     - identifier: Идентификатор продукта
    /// - Returns: Выборку тиков
    func unorderedTicks(identifier: String) -> Results<Tick> {
        let predicateFormat = "\(TickProp.identifier.rawValue) == %@"
        let predicate = NSPredicate(format: predicateFormat, identifier)
        
        let all = realm.objects(Tick.self)
        let filtered = all.filter(predicate)
        
        return filtered
    }
    
    // MARK: Deleting
    
    /// Удаляет тики, старше указаннного времени
    ///
    /// - Parameters:
    ///     - timestamp: временная метка
    func deleteTicks(olderThan timestamp: TimeInterval) {
        let predicateFormat = "\(TickProp.timestamp.rawValue) < %@"
        let predicate = NSPredicate(format: predicateFormat,
                                    NSNumber(floatLiteral: timestamp))
        
        let ticksForDeletting = realm.objects(Tick.self).filter(predicate)
        
        try! realm.write {
            realm.delete(ticksForDeletting)
        }
    }
    
}
