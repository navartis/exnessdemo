//
//  WSTick.swift
//  ExnessDemo
//
//  Created by DMITRY KULAKOV on 16/03/2017.
//  Copyright © 2017 Exness. All rights reserved.
//

import Foundation

public enum WSTickField: String, DictFieldEnum {
    case identifier = "s"
    case bid = "b"
    case ask = "a"
    case spread = "spr"
    
    public func str() -> String {
        return self.rawValue
    }
}

struct WSTick {
    let identifier: String
    let bid: Double
    let ask: Double
    let spread: Double
    
    init(identifier: String, bid: Double, ask: Double, spread: Double) {
        self.identifier = identifier
        self.bid = bid
        self.ask = ask
        self.spread = spread
    }

    init(dict: [String: Any]) throws {
        let identifier = try dict.mandatoryValue(WSTickField.identifier, String.self)
        
        let numbersFormatter = NumberFormatter()
        // because 0.12 style inspected
        numbersFormatter.locale = Locale(identifier: "en")
        
        let strBid = try dict.mandatoryValue(WSTickField.bid, String.self)
        guard let bid = numbersFormatter.number(from: strBid) as? Double else {
            throw DictObjectError.wrongFieldType(WSTickField.bid, strBid)
        }
        
        let strAsk = try dict.mandatoryValue(WSTickField.ask, String.self)
        guard let ask = numbersFormatter.number(from: strAsk) as? Double else {
            throw DictObjectError.wrongFieldType(WSTickField.ask, strAsk)
        }
        
        let strSpread = try dict.mandatoryValue(WSTickField.spread, String.self)
        guard let spread = numbersFormatter.number(from: strSpread) as? Double else {
            throw DictObjectError.wrongFieldType(WSTickField.spread, strSpread)
        }
        
        self.init(identifier: identifier, bid: bid, ask: ask, spread: spread)
    }
    
    init(json: String) throws {
        let dict = try json.dict()
        try self.init(dict: dict)
    }
    
}
