//
//  LastTickCell.swift
//  ExnessDemo
//
//  Created by DMITRY KULAKOV on 17/03/2017.
//  Copyright © 2017 Exness. All rights reserved.
//

import UIKit

class LastTickCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bidLabel: UILabel!
    @IBOutlet weak var askLabel: UILabel!
    @IBOutlet weak var spreadLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
