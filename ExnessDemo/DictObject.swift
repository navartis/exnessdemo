//
//  DKDictObject.swift
//  DKDictObject
//
//  Created by DMITRY KULAKOV on 27/12/2016.
//
//

import Foundation

public protocol DictFieldEnum {
    func str() -> String
}

public enum DictObjectError<F: DictFieldEnum, V>: Error {
    case wrongField(F, V)
    case wrongFieldType(F, V)
}

public extension Dictionary where Key: ExpressibleByStringLiteral, Value: Any {
    
    public func optionalValue<F: DictFieldEnum>(_ field: F) throws -> Any? {
        guard let key = field.str() as? Key else {
            throw DictObjectError.wrongField(field, "Unknown")
        }
        
        return self[key]
    }
    
    public func optionalTypedValue<F: DictFieldEnum, T>(_ field: F, _ type: T.Type) throws -> T? {
        let rawValue = try self.optionalValue(field)
        if rawValue == nil {
            return nil
        }
        
        guard let value = rawValue as? T else {
            throw DictObjectError.wrongFieldType(field, rawValue)
        }
        
        return value
    }
    
    public func mandatoryValue<F: DictFieldEnum, T>(_ field: F, _ type: T.Type) throws -> T {
        let rawValue = try self.optionalValue(field)
        guard let value = rawValue as? T else {
            throw DictObjectError.wrongFieldType(field, rawValue)
        }
        
        return value
    }
}

public enum JSONSerializationError: Error {
    case encoding
    case decoding
}

extension String {
    func dict() throws -> [String: Any] {
        guard let data = self.data(using: .utf8) else {
            throw JSONSerializationError.decoding
        }
        
        let rawDict = try JSONSerialization.jsonObject(with: data)
        
        guard let dict = rawDict as? [String: Any] else {
            throw JSONSerializationError.decoding
        }
        
        return dict
    }
}


