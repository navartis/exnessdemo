//
//  ExnessDemoIntegrationTests.swift
//  ExnessDemoIntegrationTests
//
//  Created by DMITRY KULAKOV on 16/03/2017.
//  Copyright © 2017 Exness. All rights reserved.
//

import XCTest
@testable import ExnessDemo
import RealmSwift
import SwiftWebSocket

class ExnessDemoIntegrationTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
//    func testExample() {
//        let exp = expectation(description: #function)
//        
//        var messageNum = 0
//        let ws = WebSocket("wss://quotes.exness.com:18400/")
//        
//        let subscribe : ()->() = {
//            let msg = "SUBSCRIBE: EURUSD,EURGBP"
//            print("send: \(msg)")
//            print()
//            ws.send(msg)
//        }
//        
////        let unsubscribeEURUSD : ()->() = {
////            let msg = "UNSUBSCRIBE: EURUSD"
////            print("send: \(msg)")
////            ws.send(msg)
////        }
//        
//        ws.event.open = {
//            print("opened")
//            subscribe()
//        }
//        ws.event.close = { code, reason, clean in
//            print("close")
//        }
//        ws.event.error = { error in
//            print("error \(error)")
//        }
//        ws.event.message = { message in
//            if let text = message as? String {
//                messageNum += 1
//                print("recv: \(text)")
//                print()
//                if messageNum == 10 {
////                    subscribe()
//                } else {
////                    send()
//                }
//            }
//        }
//
//        waitForExpectations(timeout: 30, handler: nil)
//    }
    
}
