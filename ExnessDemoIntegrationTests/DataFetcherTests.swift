//
//  DataFetcherTests.swift
//  ExnessDemo
//
//  Created by DMITRY KULAKOV on 16/03/2017.
//  Copyright © 2017 Exness. All rights reserved.
//

import XCTest
import Foundation
@testable import ExnessDemo
import RealmSwift

class DataFetcherTests: XCTestCase {
    var dataModel: DataModel!
    
    override func setUp() {
        super.setUp()
        
        Realm.Configuration.defaultConfiguration.inMemoryIdentifier = self.name
        
        var realmConfiguration = Realm.Configuration()
        realmConfiguration.inMemoryIdentifier = self.name
        let realm = try! Realm(configuration: realmConfiguration)
        
        self.dataModel = DataModel(realm: realm)
    }
    
//    func testExample() {
//        let exp = expectation(description: #function)
//        
//        dataModel.initialize(identifiers: ["EURUSD"])
//        
//        let DataFetcher = DataFetcher(dataModel: dataModel, wsURL: Defaults.wsURL)
//        DataFetcher.start()
//        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
//            DataFetcher.stop()
//            
//            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
//                DataFetcher.start()
//            }
//        }
//        
//        waitForExpectations(timeout: 30, handler: nil)
//        // This is an example of a functional test case.
//        // Use XCTAssert and related functions to verify your tests produce the correct results.
//    }
    
//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }
    
}
