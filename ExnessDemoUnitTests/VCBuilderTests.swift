//
//  VCBuilderTests.swift
//  ExnessDemo
//
//  Created by DMITRY KULAKOV on 15/03/2017.
//  Copyright © 2017 Exness. All rights reserved.
//

import XCTest
@testable import ExnessDemo

class VCBuilderTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSettingsExists() {
        let vc = VCBuilder.settings()
        
        XCTAssertNotNil(vc)
    }
    
    func testChartExists() {
        let vc = VCBuilder.chart("bla")
        
        XCTAssertNotNil(vc)
    }
    
    func testChartConfigureProductIdentifier() {
        let identifier = "qwer"
        let vc = VCBuilder.chart(identifier)
        
        XCTAssertEqual(vc?.productIdentifier, identifier)
    }
}
