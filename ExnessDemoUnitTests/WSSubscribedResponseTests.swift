//
//  WSSubscribedResponseTests.swift
//  ExnessDemo
//
//  Created by DMITRY KULAKOV on 16/03/2017.
//  Copyright © 2017 Exness. All rights reserved.
//

import XCTest
@testable import ExnessDemo

class WSSubscribedResponseTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInitByJSONWithTicks() {
        let json = "{\"subscribed_count\":9, \"subscribed_list\":{\"ticks\":[{\"s\":\"AUDUSD\",\"b\":\"0.76817\",\"bf\":0,\"a\":\"0.76831\",\"af\":1,\"spr\":\"1.4\"},{\"s\":\"EURCHF\",\"b\":\"1.06987\",\"bf\":2,\"a\":\"1.06999\",\"af\":2,\"spr\":\"1.2\"},{\"s\":\"EURGBP\",\"b\":\"0.86899\",\"bf\":2,\"a\":\"0.86917\",\"af\":0,\"spr\":\"1.8\"},{\"s\":\"EURJPY\",\"b\":\"121.751\",\"bf\":0,\"a\":\"121.767\",\"af\":2,\"spr\":\"1.6\"},{\"s\":\"EURUSD\",\"b\":\"1.07374\",\"bf\":0,\"a\":\"1.07383\",\"af\":2,\"spr\":\"0.9\"},{\"s\":\"GBPUSD\",\"b\":\"1.23550\",\"bf\":1,\"a\":\"1.23557\",\"af\":1,\"spr\":\"0.7\"},{\"s\":\"USDCAD\",\"b\":\"1.33016\",\"bf\":0,\"a\":\"1.33031\",\"af\":1,\"spr\":\"1.5\"},{\"s\":\"USDCHF\",\"b\":\"0.99633\",\"bf\":2,\"a\":\"0.99644\",\"af\":1,\"spr\":\"1.1\"},{\"s\":\"USDJPY\",\"b\":\"113.378\",\"bf\":2,\"a\":\"113.386\",\"af\":2,\"spr\":\"0.8\"}]}}"
        
        do {
            let response = try WSSubscribedResponse(json: json)
            XCTAssertEqual(response.ticks.count, 9)
        } catch {
            XCTAssertNil(error)
        }
        
    }
    
    func testInitByJSONWithoutTicks() {
        let json = "{\"subscribed_count\":0, \"subscribed_list\":{\"ticks\":null}}"
        
        do {
            let response = try WSSubscribedResponse(json: json)
            XCTAssertEqual(response.ticks.count, 0)
        } catch {
            XCTAssertNil(error)
        }

    }
    
}
