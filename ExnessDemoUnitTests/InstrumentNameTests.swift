//
//  InstrumentNameTests.swift
//  ExnessDemo
//
//  Created by DMITRY KULAKOV on 16/03/2017.
//  Copyright © 2017 Exness. All rights reserved.
//

import XCTest
@testable import ExnessDemo

class InstrumentNameTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testBySixCharactersStringIdentifier() {
        let name = InstrumentName.by("USDCHF")
        
        XCTAssertEqual(name, "USD/CHF")
    }
    
    func testByNotSixCharactersStringIdentifier() {
        let name = InstrumentName.by("bla")
        
        XCTAssertEqual(name, "bla")
    }
}
