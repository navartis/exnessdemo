//
//  WSTiksResponseTests.swift
//  ExnessDemo
//
//  Created by DMITRY KULAKOV on 16/03/2017.
//  Copyright © 2017 Exness. All rights reserved.
//

import XCTest
@testable import ExnessDemo

class WSTiksResponseTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInitWithJSON() {
        let json = "{\"ticks\":[{\"s\":\"EURCHF\",\"b\":\"1.06932\",\"bf\":0,\"a\":\"1.06944\",\"af\":2,\"spr\":\"1.2\"},{\"s\":\"USDCAD\",\"b\":\"1.32967\",\"bf\":1,\"a\":\"1.32983\",\"af\":1,\"spr\":\"1.6\"}]}"
        
        var response: WSTiksResponse!
        do {
            response = try WSTiksResponse(json: json)
        } catch {
            XCTAssertNil(error)
        }
        
        XCTAssertEqual(response.ticks.count, 2)
    }
    
    func testInitWithWrongTicksDictFormatShouldFail() {
        let dict = ["ticks": [[1: 2]]]
        
        XCTAssertThrowsError(try WSTiksResponse(dict: dict))
    }
}
