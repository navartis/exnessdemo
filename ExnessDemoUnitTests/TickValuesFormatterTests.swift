//
//  TickValuesFormatterTests.swift
//  ExnessDemo
//
//  Created by DMITRY KULAKOV on 18/03/2017.
//  Copyright © 2017 Exness. All rights reserved.
//

import XCTest
@testable import ExnessDemo

class TickValuesFormatterTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testBid() {
        let value = 0.12345678
        let strValue = TickValuesFormatter.bid(value, length: 7)
        
        XCTAssertEqual(strValue, "0.12345")
    }
    
    func testAsk() {
        let value = 12.345678
        let strValue = TickValuesFormatter.ask(value, length: 7)
        
        XCTAssertEqual(strValue, "12.3456")
    }
    
    func testSpread() {
        let value = 0.82
        let strValue = TickValuesFormatter.spread(value, length: 3)
        
        XCTAssertEqual(strValue, "0.8")
    }
}
