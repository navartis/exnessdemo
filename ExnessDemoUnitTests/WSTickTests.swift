//
//  WSTickTests.swift
//  ExnessDemo
//
//  Created by DMITRY KULAKOV on 16/03/2017.
//  Copyright © 2017 Exness. All rights reserved.
//

import XCTest
@testable import ExnessDemo

class WSTickTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInitByJSON() {
        let json = "{\"s\":\"AUDUSD\",\"b\":\"0.76914\",\"bf\":0,\"a\":\"0.76927\",\"af\":1,\"spr\":\"1.3\"}"
        
        var tick: WSTick!
        do {
            tick = try WSTick(json: json)
        } catch {
            XCTAssertNil(error)
        }
        
        XCTAssertEqual(tick.identifier, "AUDUSD")
        XCTAssertEqual(tick.bid, 0.76914)
        XCTAssertEqual(tick.ask, 0.76927)
        XCTAssertEqual(tick.spread, 1.3)

    }
}
