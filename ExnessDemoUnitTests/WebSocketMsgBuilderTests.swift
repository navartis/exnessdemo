//
//  WebSocketMsgBuilderTests.swift
//  ExnessDemo
//
//  Created by DMITRY KULAKOV on 16/03/2017.
//  Copyright © 2017 Exness. All rights reserved.
//

import XCTest
@testable import ExnessDemo

class WebSocketMsgBuilderTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSubscribeWithoutIdentifiers() {
        let msg = WebSocketMsgBuilder.subscribe(identifiers: [String]())
        
        XCTAssertEqual(msg, "SUBSCRIBE: ")
    }
    
    func testSubscribeWithIdentifiers() {
        let msg = WebSocketMsgBuilder.subscribe(identifiers: ["AB", "CD"])
        
        XCTAssertEqual(msg, "SUBSCRIBE: AB,CD")
    }
    
    func testUnsubscribeWithoutIdentifiers() {
        let msg = WebSocketMsgBuilder.unsubscribe(identifiers: [String]())
        
        XCTAssertEqual(msg, "UNSUBSCRIBE: ")
    }
    
    func testUnsubscribeWithIdentifiers() {
        let msg = WebSocketMsgBuilder.unsubscribe(identifiers: ["EF", "CD"])
        
        XCTAssertEqual(msg, "UNSUBSCRIBE: EF,CD")
    }
}
