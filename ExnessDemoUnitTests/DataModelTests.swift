//
//  DataModelTests.swift
//  ExnessDemo
//
//  Created by DMITRY KULAKOV on 15/03/2017.
//  Copyright © 2017 Exness. All rights reserved.
//

import XCTest
@testable import ExnessDemo
import RealmSwift
import RandomKit

class DataModelTests: XCTestCase {
    
    var dataModel: DataModel!
    
    override func setUp() {
        super.setUp()
        
        Realm.Configuration.defaultConfiguration.inMemoryIdentifier = self.name
        
        var realmConfiguration = Realm.Configuration()
        realmConfiguration.inMemoryIdentifier = self.name
        let realm = try! Realm(configuration: realmConfiguration)
        
        self.dataModel = DataModel(realm: realm)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInitializeAddInstruments() {
        let identifiers = ["a", "b"]
        dataModel.initialize(identifiers: identifiers)
        
        let instruments = dataModel.allInstruments()
        
        XCTAssertEqual(instruments.count, identifiers.count)
    }
    
    func testInitializeAddInstrumentsAsEnabled() {
        let identifiers = ["a", "b", "c"]
        dataModel.initialize(identifiers: identifiers)
        
        let instruments = dataModel.allInstruments()
        
        for i in instruments.enumerated(){
            let instrument = i.element
            XCTAssertTrue(instrument.enabled)
        }
    }
    
    func testInitializeAddInstrumentsWithCorrectPositions() {
        let identifiers = ["a", "b", "c"]
        dataModel.initialize(identifiers: identifiers)
        
        let instruments = dataModel.allInstruments()
        var positionsAndIdentifiers = [Int: String]()
        
        for instrument in instruments{
            positionsAndIdentifiers[instrument.position] = instrument.identifier
        }
        
        XCTAssertEqual(positionsAndIdentifiers.count, identifiers.count)
        for (position, identifier) in positionsAndIdentifiers {
            XCTAssertEqual(identifier, identifiers[position])
        }
    }
    
    func testInitializeAddLastTicks() {
        let identifiers = ["a", "b"]
        dataModel.initialize(identifiers: identifiers)
        
        let ticks = dataModel.allLastTicks()
        
        XCTAssertEqual(ticks.count, identifiers.count)
    }
    
    func testInitializeAddLastTicksAsEnabled() {
        let identifiers = ["a", "b", "c"]
        dataModel.initialize(identifiers: identifiers)
        
        let ticks = dataModel.allLastTicks()
        
        for tick in ticks {
            XCTAssertTrue(tick.enabled)
        }
    }
    
    func testInitializeAddLastTicksWithCorrectPositions() {
        let identifiers = ["a", "b", "c"]
        dataModel.initialize(identifiers: identifiers)
        
        let ticks = dataModel.allLastTicks()
        var positionsAndIdentifiers = [Int: String]()
        
        for tick in ticks {
            positionsAndIdentifiers[tick.position] = tick.identifier
        }
        
        XCTAssertEqual(positionsAndIdentifiers.count, identifiers.count)
        for (position, identifier) in positionsAndIdentifiers {
            XCTAssertEqual(identifier, identifiers[position])
        }
    }
    
    func testInstrumentWithNotExistingIdentifier() {
        let instrument = dataModel.instrument(identifier: "a")
        
        XCTAssertNil(instrument)
    }
    
    func testInstrumentWithExistingIdentifier() {
        let identifier = #function
        let identifiers = [identifier]
        dataModel.initialize(identifiers: identifiers)
        
        guard let instrument = dataModel.instrument(identifier: identifier) else {
            XCTFail("Instrument should be not nil")
            return
        }
        
        XCTAssertEqual(instrument.identifier, identifier)
    }
    
    func testSetInstrumentByIdentifierDisabled() {
        let identifier = #function
        let identifiers = [identifier]
        dataModel.initialize(identifiers: identifiers)
        
        dataModel.setInstrument(identifier: identifier, enabled: false)
        
        guard let instrument = dataModel.instrument(identifier: identifier) else {
            XCTFail("Instrument should be not nil")
            return
        }
        
        XCTAssertFalse(instrument.enabled)
    }
    
    func testSetInstrumentByIdentifierEnabled() {
        // given
        let identifier = #function
        let identifiers = [identifier]
        dataModel.initialize(identifiers: identifiers)
        
        // when
        dataModel.setInstrument(identifier: identifier, enabled: false)
        dataModel.setInstrument(identifier: identifier, enabled: true)
        
        // then
        guard let instrument = dataModel.instrument(identifier: identifier) else {
            XCTFail("Instrument should be not nil")
            return
        }
        
        XCTAssertTrue(instrument.enabled)
    }
    
    func testToggleInstrumentEnabled(){
        // given
        let identifiers = ["a", "b", "c"]
        dataModel.initialize(identifiers: identifiers)
        
        let identifierForTest = identifiers.first!
        
        dataModel.setInstrument(identifier: identifierForTest, enabled: false)
        guard let instrument = dataModel.instrument(identifier: identifierForTest) else{
            XCTFail("Instrument should be not nil")
            return
        }
        
        // when
        dataModel.toggleInstrumentEnabled(identifier: identifierForTest)
        
        // then
        XCTAssertTrue(instrument.enabled)
    }
    
    func testEnabledInstruments() {
        // given
        let identifiers = ["a", "b", "c"]
        dataModel.initialize(identifiers: identifiers)
        
        let identifierForTest = identifiers.last!
        
        // when
        dataModel.setInstrument(identifier: identifierForTest, enabled: false)
        
        //then
        let enabledInstruments = dataModel.enabledInstruments()
        var enabledIdentifiers = [String]()
        
        for instrument in enabledInstruments {
            enabledIdentifiers.append(instrument.identifier)
        }
        
        XCTAssertFalse(enabledIdentifiers.contains(identifierForTest))
    }
    
    func testChangeLastTickPositionIfFromAndToEqual() {
        let initialIdentifiers = ["0", "1", "2", "3", "4", "5"]
        dataModel.initialize(identifiers: initialIdentifiers)
        
        dataModel.changeLastTickPosition(from: 1, to: 1)
        
        let ticks = dataModel.enabledLastTicks()
        
        var resultIdentifiers = [String]()
        var resultPositions = [Int]()
        for tick in ticks {
            resultIdentifiers.append(tick.identifier)
            resultPositions.append(tick.position)
        }
        
        let expectedIdentifiers = initialIdentifiers
        let expectedPositions = [Int](0...initialIdentifiers.count-1)
        
        XCTAssertEqual(resultIdentifiers, expectedIdentifiers)
        XCTAssertEqual(resultPositions, expectedPositions)
    }
    
    func testChangeLastTickPositionUp() {
        let initialIdentifiers = ["0", "1", "2", "3", "4", "5"]
        dataModel.initialize(identifiers: initialIdentifiers)
        
        dataModel.changeLastTickPosition(from: 0, to: 3)
        
        let ticks = dataModel.enabledLastTicks()
        
        var resultIdentifiers = [String]()
        var resultPositions = [Int]()
        for tick in ticks {
            resultIdentifiers.append(tick.identifier)
            resultPositions.append(tick.position)
        }
        
        let expectedIdentifiers = ["1", "2", "3", "0", "4", "5"]
        let expectedPositions = [Int](0...initialIdentifiers.count-1)
        
        XCTAssertEqual(resultIdentifiers, expectedIdentifiers)
        XCTAssertEqual(resultPositions, expectedPositions)
    }
    
    func testChangeLastTickPositionDown() {
        let initialIdentifiers = ["0", "1", "2", "3", "4", "5"]
        dataModel.initialize(identifiers: initialIdentifiers)
        
        dataModel.changeLastTickPosition(from: 2, to: 0)
        
        let ticks = dataModel.enabledLastTicks()
        
        var resultIdentifiers = [String]()
        var resultPositions = [Int]()
        for tick in ticks {
            resultIdentifiers.append(tick.identifier)
            resultPositions.append(tick.position)
        }
        
        let expectedIdentifiers = ["2", "0", "1", "3", "4", "5"]
        let expectedPositions = [Int](0...initialIdentifiers.count-1)
        
        XCTAssertEqual(resultIdentifiers, expectedIdentifiers)
        XCTAssertEqual(resultPositions, expectedPositions)
    }
    
    func testAddTicksModifyLastTicks() {
        // given
        let identifiers = ["c", "d", "f"]
        let identifierForTest = identifiers.last!
        
        dataModel.initialize(identifiers: identifiers)
        
        // when
        let wsTick = WSTick(identifier: identifierForTest,
                            bid: Double.random(using: &Xoroshiro.default),
                            ask: Double.random(using: &Xoroshiro.default),
                            spread: Double.random(using: &Xoroshiro.default))
        
        dataModel.addTicks([wsTick])
        
        // then
        guard let lastTick = dataModel.lastTick(identifier: identifierForTest) else {
            XCTFail("Last tick should exists")
            return
        }
        
        XCTAssertEqual(lastTick.bid.value, wsTick.bid)
        XCTAssertEqual(lastTick.ask.value, wsTick.ask)
        XCTAssertEqual(lastTick.spread.value, wsTick.spread)
    }
    
    func testAddTicksAddingTicks() {
        // given
        let identifiers = ["d", "e", "g"]
        let identifierForTest = identifiers.first!
        
        dataModel.initialize(identifiers: identifiers)
        
        // when
        let wsTick = WSTick(identifier: identifierForTest,
                            bid: Double.random(using: &Xoroshiro.default),
                            ask: Double.random(using: &Xoroshiro.default),
                            spread: Double.random(using: &Xoroshiro.default))
        
        dataModel.addTicks([wsTick])
        
        // then
        let ticksCount = dataModel.unorderedTicks(identifier: identifierForTest).count
        
        XCTAssertEqual(ticksCount, 1)
    }
    
    func testDeleteTicksOlderThanTimeIntervalDeleteTicks() {
        // given
        let timeToKeep = TimeInterval(10)
        let tickTimestamp = timeToKeep - TimeInterval(1)
        let tickIdentifier = "q"
        let tick = Tick(identifier: tickIdentifier,
                        timestamp: tickTimestamp,
                        bid: 0.0,
                        ask: 0.0,
                        spread: 0.0)
        
        try! dataModel.realm.write {
            dataModel.realm.add(tick)
        }
        
        // when
        dataModel.deleteTicks(olderThan: timeToKeep)
        
        // then
        let ticksCount = dataModel.unorderedTicks(identifier: tickIdentifier).count
        
        XCTAssertEqual(ticksCount, 0)
        
    }
    
}
